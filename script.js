

const computeTax = function() {
    let sum_total = document.getElementById('sumTotal').value;
    let tax_value = document.getElementById('taxPercentage').value;

    if (document.getElementById('forwardTax').checked == true) {
        let total_tax_paid = (tax_value / 100) * sum_total;
        let total_plus_tax = +total_tax_paid + +sum_total;
        return (
            document.getElementById('calcTax').value = total_tax_paid.toFixed(2) + ' €',
            document.getElementById('calcResult').value = total_plus_tax.toFixed(2) + ' €'
        );
    }
    else if (document.getElementById('reverseTax').checked == true) {
        let total_after_tax = sum_total / (1 + (tax_value / 100));
        let total_tax_deducted = -1 * (total_after_tax - sum_total);
        return (
            document.getElementById('calcTax').value = total_tax_deducted.toFixed(2) + ' €',
            document.getElementById('calcResult').value = total_after_tax.toFixed(2) + ' €'
        );
    }
    else {
        return document.getElementById('calcResult').value = 'Invalid input(s)';
    }
}

/*
class ComputeTax {
    constructor(sumTotal, taxValue){
        this.sumTotal = sumTotal;
        this.taxValue = taxValue;
    }
    forwardCalcTax(){
        let total_tax_paid = (this.taxValue / 100) * this.sumTotal;
        return document.getElementById('calcTax').value = total_tax_paid;
        }
    forwardCalcResult(){
        let total_tax_paid = (this.taxValue / 100) * this.sumTotal;
        let total_plus_tax = (+total_tax_paid + +this.sumTotal).toFixed(2);
        return document.getElementById('calcResult').value = total_plus_tax;
    }
    
    reverseCalcTax(){
        let total_after_tax = this.sumTotal / (1 + (this.taxValue / 100));
        let total_tax_deducted = (total_after_tax - this.sumTotal).toFixed(2);
        return document.getElementById('calcTax').value = total_tax_deducted;
    }
    reverseCalcResult(){
        let total_after_tax = (this.sumTotal / (1 + (this.taxValue / 100))).toFixed(2);
        return document.getElementById('calcResult').value = total_after_tax;
    }

}


let amtInput = document.getElementById('sumTotal').value;
let taxInput = document.getElementById('taxPercentage').value;
let calc = new ComputeTax(amtInput, taxInput);
*/